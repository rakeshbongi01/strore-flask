import sqlite3
from flask_jwt import jwt_required
from flask_restful import Resource,reqparse

from models.item import ItemModel

class Item(Resource):
    parser=reqparse.RequestParser()
    parser.add_argument('price',type=float,required=True,help="this field is required")
    @jwt_required()
    def get(self,name):
        item=ItemModel.find_by_name(name)
        if item:
            return item.json()
        return {'message':'Item not found'},404



    def post(self,name):
        if ItemModel.find_by_name(name):
            return {'message':"An Item with name '{}',already exists".format(name)},400

        data=Item.parser.parse_args()
        item=ItemModel(name,data['price'])

        try:
            item.insert()
        except:
            return {"message":"An Error Occured While Inserting the Item"},500

        return item.json(),201




    def delete(self,name):

        connection=sqlite3.connect("data.db")
        cursor=connection.cursor()

        # dont miss WHERE otherwise it will delete all the data from items
        query="DELETE FROM items WHERE name=?"
        cursor.execute(query,(name,))
        connection.commit()
        connection.close()

        return {'message':'Item Deleted'}

    def put(self,name):
        data=Item.parser.parse_args()
        item=ItemModel.find_by_name(name)
        updated_item=ItemModel(name,data['price'])

        if item is None:
            try:
                updated_item.insert()
            except:
                return{"message":"Exception while inserting Item"},500
        else:
            try:
                updated_item.update()
            except:
                return{"message":"Exception while updating Item"},500
        return updated_item.json()



class ItemList(Resource):
    def get(self):
        connection=sqlite3.connect("data.db")
        cursor=connection.cursor()

        # dont miss WHERE otherwise it will UPDATE all the data from items
        query="SELECT * FROM items"
        result=cursor.execute(query)

        items=[]
        for row in result:
            items.append({'name':row[0],'price':row[1]})

        connection.close()
        return {'items':items}
