from flask import Flask
from flask_restful import Api
from flask_jwt import JWT

from security import authenticate,identity
from resources.user import UserRegister
from resources.item import Item,ItemList

from db import db
db.init_app(app)

app=Flask(__name__)
# stops tracking SQLAlchemy in flask  because we are have SQLAlchemy main library has its own modification traker
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
api=Api(app)
app.secret_key='Flask'

jwt=JWT(app,authenticate,identity)


api.add_resource(Item,'/item/<string:name>')
api.add_resource(ItemList,'/items')
api.add_resource(UserRegister,'/register')

if __name__=='__main__':
    app.run(port=5000)
